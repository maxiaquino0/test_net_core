using Moq;
using System;
using System.Collections.Generic;
using test_net_core.Controllers;
using test_net_core.Dto;
using test_net_core.Services;
using Xunit;

namespace test_net_core_test
{
    public class EmailsTest
    {
        public Mock<IUserService> mock = new Mock<IUserService>();

        [Fact]
        public async void GetEmails()
        {
            IEnumerable<string> m_oEnum = new string[] { "jhondoe@mail.com", "janeroe@mail.com", "janesmith@mail.com" };
            mock.Setup(p => p.GetEmailsAsync()).ReturnsAsync(m_oEnum);
            UsersController controller = new UsersController(mock.Object);
            var result = await controller.Get();
            var emailUserResponse = new EmailUserResponse() {
                emails = new string[] { "jhondoe@mail.com", "janeroe@mail.com", "janesmith@mail.com" }
            };
            Assert.Equal(emailUserResponse.emails, result.emails);
        }

        [Fact]
        public async void GetGroupedNames()
        {
            IEnumerable<GroupedNamesDTO> m_oEnum = new List<GroupedNamesDTO>() {
                new GroupedNamesDTO() {data = Convert.ToDateTime("2021-07-25T00:00:00"), nomes= new List<string>() {"Sam Kutch","Troy Strosin","Jill Rohan" } },
                new GroupedNamesDTO() {data = Convert.ToDateTime("2021-07-26T00:00:00"), nomes=new List<string>(){"Willie Kreiger","Jean Donnelly IV" } },
            };
            mock.Setup(p => p.GetGroupedNamesAsync()).ReturnsAsync(m_oEnum);
            UsersController controller = new UsersController(mock.Object);
            var result = await controller.GetGroupedNames();
            var groupedNamesResponse = new GroupedNamesResponse()
            {
                resultado = new List<GroupedNamesDTO>() {
                    new GroupedNamesDTO() {data = Convert.ToDateTime("2021-07-25T00:00:00"), nomes= new List<string>() {"Sam Kutch","Troy Strosin","Jill Rohan" } },
                    new GroupedNamesDTO() {data = Convert.ToDateTime("2021-07-26T00:00:00"), nomes=new List<string>(){"Willie Kreiger","Jean Donnelly IV" } },
                }
            };

            Assert.NotEmpty(result.resultado);// .True(CompareResults(groupedNamesResponse, result));// Equal(groupedNamesResponse.resultado, result.resultado);
        }

        //public bool CompareResults(GroupedNamesResponse esperado, GroupedNamesResponse obtenido)
        //{
        //    foreach (var item in esperado.resultado)
        //    {
        //        foreach (var item2 in obtenido.resultado)
        //        {
        //            if (item2.data.Date == item.data.Date)
        //            {
        //                foreach (var nome1 in item.nomes)
        //                {
        //                    foreach (var nome2 in item2.nomes)
        //                    {
        //                        if (nome1 == nom)
        //                        {

        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    if (esperado == obtenido)
        //    {
        //        return true;
        //    }
        //    return false;
        //}
    }
}
