﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using test_net_core.Models;

namespace test_net_core.Repository
{
    public static class UserRepository
    {
        public static User Get(string username, string password)
        {
            var users = new List<User>();
            users.Add(new User { Id = 1, Username = "jhon.doe", Password = "jdoe", Role = "manager" });
            users.Add(new User { Id = 2, Username = "jane.doe", Password = "jadoe", Role = "employee" });
            return users.Where(x => x.Username.ToLower() == username.ToLower() && x.Password == x.Password).FirstOrDefault();
        }
    }
}
