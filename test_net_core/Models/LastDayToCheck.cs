﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test_net_core.Models
{
    public class LastDayToCheck
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
    }
}
