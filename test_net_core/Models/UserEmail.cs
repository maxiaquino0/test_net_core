﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test_net_core.Models
{
    public class UserEmail
    {
        public DateTime createdAt { get; set; }
        public string name { get; set; }
        public string avatar { get; set; }
        public string mail { get; set; }
        public string id { get; set; }
    }
}
