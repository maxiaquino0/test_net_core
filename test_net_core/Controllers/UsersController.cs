﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using test_net_core.Data;
using test_net_core.Dto;
using test_net_core.Models;
using test_net_core.Repository;
using test_net_core.Services;

namespace test_net_core.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        //private static readonly string[] Summaries = new[]
        //{
        //    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        //};


        private readonly IUserService _userService;


        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody] UserDto model)
        {
            var user = UserRepository.Get(model.Username, model.Password);

            if (user == null)
                return NotFound(new { message = "User or password invalid" });

            var token = TokenService.CreateToken(user);
            user.Password = "";
            return new
            {
                user = user,
                token = token
            };
        }

        [HttpGet]
        public async Task<EmailUserResponse> Get()
        {
            var result = await _userService.GetEmailsAsync();
            EmailUserResponse emails = new EmailUserResponse()
            {
                emails = result
            };
            return emails;
        }

        [HttpGet, Route("GetGroupedNames")]
        public async Task<GroupedNamesResponse> GetGroupedNames()
        {
            var result = await _userService.GetGroupedNamesAsync();
            GroupedNamesResponse groups = new GroupedNamesResponse()
            {
                resultado = result
            };
            return groups;
        }

        [Authorize(Roles = "manager")]
        [HttpGet, Route("GetWithAuthentication")]
        public async Task<EmailUserResponse> GetWithAuthentication()
        {
            var result = await _userService.GetEmailsAsync();
            EmailUserResponse emails = new EmailUserResponse()
            {
                emails = result
            };
            return emails;
        }
    }
}
