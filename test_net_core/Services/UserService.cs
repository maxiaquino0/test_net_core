﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using test_net_core.Data;
using test_net_core.Dto;
using test_net_core.Models;

namespace test_net_core.Services
{
    public class UserService : IUserService
    {
        private readonly test_net_coreContext _appDbContext;
        static readonly HttpClient _client = new HttpClient();
        private DateTime _today = DateTime.Now.Date;
        public UserService(test_net_coreContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<IEnumerable<string>> GetEmailsAsync()
        {
            HttpResponseMessage response = await _client.GetAsync("https://6064ac2bf09197001778660d.mockapi.io/api/test-api");

            DateTime current = DateTime.Now.Date;

            var lastDayToCheck = _appDbContext.LastDayToCheck.FirstOrDefault();

            if (lastDayToCheck != null && current == lastDayToCheck.Date && _appDbContext.UserEmail.ToList().Count > 0)
            {
                return _appDbContext.UserEmail.ToList().Select(x => x.mail);
            }
            else
            {
                if (lastDayToCheck == null)
                {
                    _appDbContext.LastDayToCheck.Add(new LastDayToCheck() { Date = current });
                }
                else
                {
                    lastDayToCheck.Date = current;
                }
                _appDbContext.UserEmail.RemoveRange(_appDbContext.UserEmail.ToList());

                List<UserEmail> usersResponse = JsonConvert.DeserializeObject<List<UserEmail>>(response.Content.ReadAsStringAsync().Result);

                _appDbContext.AddRange(usersResponse);
                _appDbContext.SaveChanges();

                return _appDbContext.UserEmail.ToList().Select(x => x.mail);
            }

        }

        public async Task<IEnumerable<GroupedNamesDTO>> GetGroupedNamesAsync()
        {
            HttpResponseMessage response = await _client.GetAsync("https://6064ac2bf09197001778660d.mockapi.io/api/test-api");

            DateTime current = DateTime.Now.Date;

            var lastDayToCheck = _appDbContext.LastDayToCheck.FirstOrDefault();

            if (lastDayToCheck != null && current == lastDayToCheck.Date && _appDbContext.UserEmail.ToList().Count > 0)
            {
                return _appDbContext.UserEmail.ToList().GroupBy(x => x.createdAt.Date).ToList().Select(x => new GroupedNamesDTO { data = x.Key, nomes = x.Select(n => n.name) });
            }
            else
            {
                if (lastDayToCheck == null)
                {
                    _appDbContext.LastDayToCheck.Add(new LastDayToCheck() { Date = current });
                }
                else
                {
                    lastDayToCheck.Date = current;
                }
                _appDbContext.UserEmail.RemoveRange(_appDbContext.UserEmail.ToList());

                List<UserEmail> usersResponse = JsonConvert.DeserializeObject<List<UserEmail>>(response.Content.ReadAsStringAsync().Result);

                _appDbContext.AddRange(usersResponse);
                _appDbContext.SaveChanges();

                return _appDbContext.UserEmail.ToList().GroupBy(x => x.createdAt.Date).ToList().Select(x => new GroupedNamesDTO { data = x.Key, nomes = x.Select(n => n.name) });
            }
        }
    }
}
