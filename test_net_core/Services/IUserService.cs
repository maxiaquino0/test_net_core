﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using test_net_core.Dto;

namespace test_net_core.Services
{
    public interface IUserService
    {
        Task<IEnumerable<string>> GetEmailsAsync();
        Task<IEnumerable<GroupedNamesDTO>> GetGroupedNamesAsync();
    }
}
