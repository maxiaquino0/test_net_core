﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test_net_core.Dto
{
    public class GroupedNamesResponse
    {
        public IEnumerable<GroupedNamesDTO> resultado { get; set; }
    }
}
