﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test_net_core.Dto
{
    public class EmailUserResponse
    {
        public IEnumerable<string> emails { get; set; }
    }
}
