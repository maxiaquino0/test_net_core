﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test_net_core.Dto
{
    public class GroupedNamesDTO
    {
        public DateTime data { get; set; }
        public IEnumerable<string> nomes { get; set; }
    }
}
